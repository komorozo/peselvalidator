package com.sda.peselValidator;

import java.util.Scanner;


public class PeselValidator {


    public boolean sprawdz(String pesel) {
        int[] wagi = {1, 3, 7, 9, 1, 3, 7, 9, 1, 3, 1};
        if (pesel.length() != 11) return false;
        int suma = 0;
        for (int i = 0; i < 11; i++) {
            suma = suma + Integer.parseInt(pesel.substring(i, i + 1)) * wagi[i];
        }
        return (suma % 10 == 0);
    }

    public void czeckDate(String pesel) {
        int dzien = 0;
        int miesiac = 0;
        int rok = 0;
        dzien = dzien + Integer.parseInt(pesel.substring(4, 6));
        miesiac = miesiac + Integer.parseInt(pesel.substring(2, 4));
        rok = rok + Integer.parseInt(pesel.substring(0, 2));
        if (miesiac <= 12) {
            System.out.println("Urodziłeś/aś się: " + dzien + "." + miesiac + "." + "19" + rok);
        }
        if (miesiac > 12) {
            if(miesiac == 21){
                miesiac = 1;
            }
            if(miesiac == 22){
                miesiac = 2;
            }
            if(miesiac == 23){
                miesiac = 3;
            }
            if(miesiac == 24){
                miesiac = 4;
            }
            if(miesiac == 25){
                miesiac = 5;
            }
            if(miesiac == 26){
                miesiac = 6;
            }
            if(miesiac == 27){
                miesiac = 7;
            }
            if(miesiac == 28){
                miesiac = 8;
            }
            if(miesiac == 29){
                miesiac = 9;
            }
            if(miesiac == 30){
                miesiac = 10;
            }
            if(miesiac == 31){
                miesiac = 11;
            }
            if(miesiac == 32){
                miesiac = 12;
            }
            System.out.println("Urodziłeś/aś się: " + dzien + "." + miesiac + "." + "20" + rok);
        }
    }

    public void jakaPlec(String pesel) {
        int liczbaPlec;
        String plec;
        liczbaPlec = Integer.parseInt(pesel.substring(9, 10));
        if (liczbaPlec % 2 == 0) {
            plec = "kobieta";
        } else {
            plec = "mężczyzna";
        }
        System.out.println(plec);
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println(" podaj nr pesel: ");

        try {
            String pesel = sc.nextLine();

            PeselValidator mojPesel = new PeselValidator();
            mojPesel.sprawdz(pesel);
            if (mojPesel.sprawdz(pesel) == true) {
                System.out.println("poprawny pesel");
            } else {
                System.out.println("niepoprawny pesel");
            }

            mojPesel.jakaPlec(pesel);
            mojPesel.czeckDate(pesel);
        } catch (NumberFormatException e) {
            System.out.println("wprowadziłes niepoprawny format pesel");
        } catch (StringIndexOutOfBoundsException e) {
            System.out.println("wprowadziłes niepoprawny format pesel");
        }
    }
}